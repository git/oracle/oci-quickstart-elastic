# oci-quickstart-elastic

These are Terraform modules that deploy [Elastic](https://www.elastic.co/products/) on [Oracle Cloud Infrastructure (OCI)](https://cloud.oracle.com/en_US/cloud-infrastructure).  They are developed jointly by Oracle and Elastic. For instructions on how to use this material and details on getting support from the vendor that maintains this material, please contact them directly.

* [simple](simple) deploys Elasticsearch, Kibana, and Logstash on a VM
* [cluster](cluster) deploys a highly available cluster with Elasticsearch and Kibana

Please follow the instructions in [simple](simple) or [cluster](cluster) folders to deploy.
